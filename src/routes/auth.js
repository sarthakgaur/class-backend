import express from "express";
import bcrypt from "bcrypt";
import { body, validationResult } from "express-validator";

import * as db from "../db/db.js";
import jwtGenerator from "../utils/jwtGenerator.js";
import ROLE from "../data/roles.js";

const router = express.Router();

/**
 * @swagger
 * /auth/register:
 *  post:
 *    summary: Make a new account.
 *    tags:
 *      - auth
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              username:
 *                type: string
 *              email:
 *                type: string
 *              password:
 *                type: string
 *              role:
 *                type: string
 *    responses:
 *      '200':
 *        description: Created
 *      '400':
 *        description: Invalid body
 *      '401':
 *        description: User exists
 */
router.post(
  "/register",
  body("username").isLength({ min: 3 }),
  body("email").isEmail(),
  body("password").isLength({ min: 5 }),
  body("role").exists(),
  async (req, res) => {
    try {
      // Check if input is valid.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const { username, email, password, role } = req.body;

      // Check if user exists.
      const userEmailCheck = await db.query(
        "SELECT * FROM users WHERE user_email = $1",
        [email]
      );

      const usernameCheck = await db.query(
        "SELECT * FROM users WHERE user_name = $1",
        [username]
      );

      if (userEmailCheck.rowCount !== 0 || usernameCheck.rowCount !== 0) {
        return res.status(401).json("User already exists");
      }

      // Assign role_id.
      const roleID = parseInt(role === "teacher" ? ROLE.TEACHER : ROLE.STUDENT);

      // Bcrypt the user password.
      const saltRound = 10;
      const salt = await bcrypt.genSalt(saltRound);
      const bcryptPassword = await bcrypt.hash(password, salt);

      // Enter the new user inside our database.
      const query = `
        INSERT INTO users (user_name, user_email, user_password, role_id)
        VALUES ($1, $2, $3, $4)
        RETURNING *
      `;

      const newUser = await db.query(query, [
        username,
        email,
        bcryptPassword,
        roleID,
      ]);

      // Generating our jwt token.
      const token = jwtGenerator(newUser.rows[0].user_uuid);

      res.json({ token });
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error");
    }
  }
);

/**
 * @swagger
 * /auth/login:
 *  post:
 *    summary: Account login.
 *    tags:
 *      - auth
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *    responses:
 *      '200':
 *        description: Created
 *      '400':
 *        description: Invalid body
 *      '401':
 *        description: Incorrect email or password
 */
router.post(
  "/login",
  body("email").isEmail(),
  body("password").isLength({ min: 5 }),
  async (req, res) => {
    try {
      // Check if input is valid.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const { email, password } = req.body;

      // Check if user doesn't exist.
      const user = await db.query("SELECT * FROM users WHERE user_email = $1", [
        email,
      ]);

      if (user.rowCount === 0) {
        return res.status(401).json("Password or email is incorrect");
      }

      // Check if the password is correct.
      const validPassword = await bcrypt.compare(
        password,
        user.rows[0].user_password
      );

      if (!validPassword) {
        return res.status(401).json("Password or email is incorrect");
      }

      // Give them the jwt token.
      const token = jwtGenerator(user.rows[0].user_uuid);

      res.json({ token });
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error");
    }
  }
);

export default router;
