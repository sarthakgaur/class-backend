import express from "express";
import { body, validationResult } from "express-validator";

import * as db from "../db/db.js";
import getStudentDetails from "../utils/studentDetails.js";

import tokenCheck from "../middlewares/tokenCheck.js";
import userCheck from "../middlewares/userCheck.js";
import teacherCheck from "../middlewares/teacherCheck.js";

const router = express.Router();

/**
 * @swagger
 * /user/{username}/classes/{slug}/students:
 *  post:
 *    summary: Add a new student in a teacher's class.
 *    description: Teacher account required. The student should have an account.
 *    tags:
 *      - students
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *      - name: slug
 *        in: path
 *        required: true
 *        description: Class slug
 *        schema:
 *          type: string
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              student_username:
 *                type: string
 *              grade:
 *                type: string
 *              remarks:
 *                type: string
 *    responses:
 *      '200':
 *        description: Created
 *      '400':
 *        description: Invalid body
 */
router.post(
  "/",
  tokenCheck,
  userCheck,
  teacherCheck,
  body("student_username").exists(),
  async (req, res) => {
    try {
      // Check if input is valid.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const slug = req.slug;
      const teacherUUID = req.userUUID;
      const studentUsername = req.body.student_username;
      const grade = req.body.grade;
      const remarks = req.body.remarks;

      // Get classUUID.
      let result = await db.query(
        "SELECT * FROM classes WHERE (slug = $1 AND teacher_uuid = $2)",
        [slug, teacherUUID]
      );

      const classUUID = result.rows[0].class_uuid;

      // Get studentUUID.
      result = await db.query("SELECT * FROM users WHERE user_name = $1", [
        studentUsername,
      ]);

      const studentUUID = result.rows[0].user_uuid;

      // Register this student for this class.
      let query = `
        INSERT INTO student_registrations (class_uuid, student_uuid, grade, remarks)
        VALUES ($1, $2, $3, $4)
        RETURNING *
      `;

      result = await db.query(query, [classUUID, studentUUID, grade, remarks]);
      const regUUID = result.rows[0].reg_uuid;

      // Get readable result.
      result = await getStudentDetails("reg_uuid=$1", [regUUID]);

      res.json(result.rows[0]);
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error.");
    }
  }
);

/**
 * @swagger
 * /user/{username}/classes/{slug}/students:
 *  get:
 *    summary: Get all students in a teacher's class.
 *    description: Teacher account required.
 *    tags:
 *      - students
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *      - name: slug
 *        in: path
 *        required: true
 *        description: Class slug
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: OK
 */
router.get("/", tokenCheck, userCheck, teacherCheck, async (req, res) => {
  try {
    const slug = req.slug;
    const teacherUUID = req.userUUID;

    // Get classUUID.
    let result = await db.query(
      "SELECT * FROM classes WHERE (slug = $1 AND teacher_uuid = $2)",
      [slug, teacherUUID]
    );

    const classUUID = result.rows[0].class_uuid;

    // Get all students from this class.
    result = await getStudentDetails("class_uuid=$1", [classUUID]);

    res.json(result.rows);
  } catch (error) {
    console.error(error);
    res.status(500).send("Server Error.");
  }
});

/**
 * @swagger
 * /user/{username}/classes/{slug}/students/{student}:
 *  get:
 *    summary: Get a student in a teacher's class.
 *    description: Teacher account required.
 *    tags:
 *      - students
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *      - name: slug
 *        in: path
 *        required: true
 *        description: Class slug
 *        schema:
 *          type: string
 *      - name: student
 *        in: path
 *        required: true
 *        description: User name of the student.
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: OK
 */
router.get(
  "/:student",
  tokenCheck,
  userCheck,
  teacherCheck,
  async (req, res) => {
    try {
      const slug = req.slug;
      const teacherUUID = req.userUUID;
      const studentUsername = req.params.student;

      // Get classUUID.
      let result = await db.query(
        "SELECT * FROM classes WHERE (slug = $1 AND teacher_uuid = $2)",
        [slug, teacherUUID]
      );

      const classUUID = result.rows[0].class_uuid;

      // Get studentUUID.
      result = await db.query("SELECT * FROM users WHERE user_name = $1", [
        studentUsername,
      ]);

      const studentUUID = result.rows[0].user_uuid;

      // Get this student from this class.
      result = await getStudentDetails("class_uuid=$1 AND student_uuid=$2", [
        classUUID,
        studentUUID,
      ]);

      if (result.rowCount === 0) {
        return res.status(400).json("Student does not exist.");
      }

      res.json(result.rows[0]);
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error.");
    }
  }
);

/**
 * @swagger
 * /user/{username}/classes/{slug}/students/{student}:
 *  put:
 *    summary: Update a student in a teacher's class.
 *    description: Teacher account required.
 *    tags:
 *      - students
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *      - name: slug
 *        in: path
 *        required: true
 *        description: Class slug
 *        schema:
 *          type: string
 *      - name: student
 *        in: path
 *        required: true
 *        description: User name of the student.
 *        schema:
 *          type: string
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              grade:
 *                type: string
 *              remarks:
 *                type: string
 *    responses:
 *      '200':
 *        description: Updated
 */
router.put(
  "/:student",
  tokenCheck,
  userCheck,
  teacherCheck,
  async (req, res) => {
    try {
      const slug = req.slug;
      const teacherUUID = req.userUUID;
      const studentUsername = req.params.student;

      // Get classUUID.
      let result = await db.query(
        "SELECT * FROM classes WHERE (slug = $1 AND teacher_uuid = $2)",
        [slug, teacherUUID]
      );

      const classUUID = result.rows[0].class_uuid;

      // Get studentUUID.
      result = await db.query("SELECT * FROM users WHERE user_name = $1", [
        studentUsername,
      ]);

      const studentUUID = result.rows[0].user_uuid;

      // Update the student's entry in the students table.
      const grade = req.body.grade;
      const remarks = req.body.remarks;

      const query = `
        UPDATE student_registrations 
        SET grade = $1,
          remarks = $2
        WHERE (class_uuid = $3 AND student_uuid = $4)
        RETURNING *
      `;

      result = await db.query(query, [grade, remarks, classUUID, studentUUID]);
      const regUUID = result.rows[0].reg_uuid;

      // Get readable result.
      result = await getStudentDetails("reg_uuid=$1", [regUUID]);

      res.json(result.rows[0]);
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error.");
    }
  }
);

/**
 * @swagger
 * /user/{username}/classes/{slug}/students/{student}:
 *  delete:
 *    summary: delete a student in a teacher's class.
 *    description: Teacher account required.
 *    tags:
 *      - students
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *      - name: slug
 *        in: path
 *        required: true
 *        description: Class slug
 *        schema:
 *          type: string
 *      - name: student
 *        in: path
 *        required: true
 *        description: User name of the student.
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: deleted
 *      '400':
 *        description: Registration does not exist
 */
router.delete(
  "/:student",
  tokenCheck,
  userCheck,
  teacherCheck,
  async (req, res) => {
    try {
      const slug = req.slug;
      const teacherUUID = req.userUUID;
      const studentUsername = req.params.student;

      // Get classUUID.
      let result = await db.query(
        "SELECT * FROM classes WHERE (slug = $1 AND teacher_uuid = $2)",
        [slug, teacherUUID]
      );

      const classUUID = result.rows[0].class_uuid;

      // Get studentUUID.
      result = await db.query("SELECT * FROM users WHERE user_name = $1", [
        studentUsername,
      ]);

      const studentUUID = result.rows[0].user_uuid;

      // Delete this student's registration for this class.
      result = await db.query(
        "DELETE FROM student_registrations WHERE (class_uuid=$1 AND student_uuid=$2)",
        [classUUID, studentUUID]
      );

      if (result.rowCount === 0) {
        return res.status(400).json("Registration does not exist.");
      }

      res.json("Student registration deleted successfully.");
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error.");
    }
  }
);

export default router;
