import express from "express";
import { body, validationResult } from "express-validator";

import * as db from "../db/db.js";

import tokenCheck from "../middlewares/tokenCheck.js";
import userCheck from "../middlewares/userCheck.js";
import getStudentDetails from "../utils/studentDetails.js";

const router = express.Router();

/**
 * @swagger
 * /user/{username}/registrations/:
 *  post:
 *    summary: Make a new registration.
 *    tags:
 *      - registrations
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the student.
 *        schema:
 *          type: string
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              teacher_username:
 *                type: string
 *              class_slug:
 *                type: string
 *    responses:
 *      '200':
 *        description: Registered
 *      '400':
 *        description: Invalid body
 */
router.post(
  "/",
  tokenCheck,
  userCheck,
  body("teacher_username").exists(),
  body("class_slug").isSlug(),
  async (req, res) => {
    try {
      // Check if input is valid.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const studentUUID = req.userUUID;
      const teacherUsername = req.body.teacher_username;
      const slug = req.body.class_slug;

      // Get teacherUUID.
      let result = await db.query("SELECT * FROM users WHERE user_name = $1", [
        teacherUsername,
      ]);

      const teacherUUID = result.rows[0].user_uuid;

      // Get classUUID.
      result = await db.query(
        "SELECT * FROM classes WHERE (slug = $1 AND teacher_uuid = $2)",
        [slug, teacherUUID]
      );

      if (result.rowCount === 0) {
        return res.status(400).json("class does not exist.");
      }

      const classUUID = result.rows[0].class_uuid;

      // Register this student for this class.
      let query = `
        INSERT INTO student_registrations (class_uuid, student_uuid)
        VALUES ($1, $2)
        RETURNING *
      `;

      result = await db.query(query, [classUUID, studentUUID]);
      const regUUID = result.rows[0].reg_uuid;

      // Get readable result.
      result = await getStudentDetails("reg_uuid=$1", [regUUID]);

      res.json(result.rows[0]);
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error.");
    }
  }
);

/**
 * @swagger
 * /user/{username}/registrations/:
 *  get:
 *    summary: Get all registrations of a student.
 *    tags:
 *      - registrations
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the student.
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: Created
 */
router.get("/", tokenCheck, userCheck, async (req, res) => {
  try {
    const studentUUID = req.userUUID;

    // Get all registrations for this student.
    const result = await getStudentDetails("student_uuid=$1", [studentUUID]);

    res.json(result.rows);
  } catch (error) {
    console.error(error);
    res.status(500).send("Server Error.");
  }
});

/**
 * @swagger
 * /user/{username}/registrations/{reg_uuid}:
 *  get:
 *    summary: Get a registration of a student.
 *    tags:
 *      - registrations
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the student.
 *        schema:
 *          type: string
 *      - name: reg_uuid
 *        in: path
 *        required: true
 *        description: Registration uuid.
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: Created
 *      '400':
 *        description: Registration does not exist
 */
router.get("/:reg_uuid", tokenCheck, userCheck, async (req, res) => {
  try {
    const regUUID = req.params.reg_uuid;

    // Get this registration for this student.
    const result = await getStudentDetails("reg_uuid=$1", [regUUID]);

    if (result.rowCount === 0) {
      return res.status(400).json("Registration does not exist.");
    }

    res.json(result.rows[0]);
  } catch (error) {
    console.error(error);
    res.status(500).send("Server Error.");
  }
});

/**
 * @swagger
 * /user/{username}/registrations/{reg_uuid}:
 *  delete:
 *    summary: Delete a registration of a student.
 *    tags:
 *      - registrations
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the student.
 *        schema:
 *          type: string
 *      - name: reg_uuid
 *        in: path
 *        required: true
 *        description: Registration uuid.
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: Created
 *      '400':
 *        description: Registration does not exist
 */
router.delete("/:reg_uuid", tokenCheck, userCheck, async (req, res) => {
  try {
    const regUUID = req.params.reg_uuid;

    // Delete this student's registration for this class.
    const result = await db.query(
      "DELETE FROM student_registrations WHERE (reg_uuid=$1)",
      [regUUID]
    );

    if (result.rowCount === 0) {
      return res.status(400).json("Registration does not exist.");
    }

    res.json("Student registration deleted successfully.");
  } catch (error) {
    console.error(error);
    res.status(500).send("Server Error.");
  }
});

export default router;
