import express from "express";
import { body, validationResult } from "express-validator";

import * as db from "../db/db.js";
import studentRouter from "./students.js";
import ROLES from "../data/roles.js";

import tokenCheck from "../middlewares/tokenCheck.js";
import userCheck from "../middlewares/userCheck.js";
import teacherCheck from "../middlewares/teacherCheck.js";
import slugParam from "../middlewares/slugParam.js";

const router = express.Router();

/**
 * @swagger
 * /user/{username}/classes:
 *  post:
 *    summary: Make a new class.
 *    description: Teacher account required
 *    tags:
 *      - classes
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              class_name:
 *                type: string
 *              class_description:
 *                type: string
 *              slug:
 *                type: string
 *    responses:
 *      '200':
 *        description: Created
 *      '400':
 *        description: Invalid body
 */
router.post(
  "/",
  tokenCheck,
  userCheck,
  teacherCheck,
  body("class_name").isLength({ min: 3 }),
  body("class_description").exists(),
  body("slug").isSlug(),
  async (req, res) => {
    try {
      // Check if input is valid.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const teacherUUID = req.userUUID;
      const className = req.body.class_name;
      const classDescription = req.body.class_description;
      const slug = req.body.slug;

      // Create a new class.
      const query = `
        INSERT INTO classes (class_name, class_description, slug, teacher_uuid)
        VALUES ($1, $2, $3, $4)
        RETURNING *
      `;

      const result = await db.query(query, [
        className,
        classDescription,
        slug,
        teacherUUID,
      ]);

      res.json(result.rows[0]);
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error.");
    }
  }
);

/**
 * @swagger
 * /user/{username}/classes:
 *  get:
 *    summary: Get all classes of a teacher.
 *    description: Account required
 *    tags:
 *      - classes
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: Ok
 */
router.get("/", tokenCheck, async (req, res) => {
  try {
    const username = req.username;

    // Check if username is valid.
    let result = await db.query("SELECT * FROM users WHERE user_name = $1", [
      username,
    ]);

    if (result.rowCount === 0 || result.rows[0].role_id !== ROLES.TEACHER) {
      return res.status(400).json("Invalid username");
    }

    const teacherUUID = result.rows[0].user_uuid;

    // Get all classes.
    result = await db.query(
      "SELECT class_name, class_description FROM classes WHERE teacher_uuid=$1",
      [teacherUUID]
    );

    if (result.rowCount === 0) {
      return res.status(400).json("No classes found.");
    }

    res.json(result.rows);
  } catch (error) {
    console.error(error);
    res.status(500).send("Server Error.");
  }
});

/**
 * @swagger
 * /user/{username}/classes/{slug}:
 *  get:
 *    summary: Get a class of a teacher.
 *    description: Account required
 *    tags:
 *      - classes
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *      - name: slug
 *        in: path
 *        required: true
 *        description: Class slug
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: Ok
 *      '400':
 *        description: Invalid Params
 */
router.get("/:slug", tokenCheck, async (req, res) => {
  try {
    const username = req.username;
    const slug = req.params.slug;

    // Check if username is valid.
    let result = await db.query("SELECT * FROM users WHERE user_name = $1", [
      username,
    ]);

    if (result.rowCount === 0 || result.rows[0].role_id !== ROLES.TEACHER) {
      return res.status(400).json("Invalid username");
    }

    const teacherUUID = result.rows[0].user_uuid;

    // Check if class exists.
    const query = `
      SELECT class_name, class_description, slug
      FROM classes
      WHERE (teacher_uuid=$1 AND slug=$2)
    `;

    result = await db.query(query, [teacherUUID, slug]);

    if (result.rowCount === 0) {
      return res.status(400).json("No class found.");
    }

    res.json(result.rows[0]);
  } catch (error) {
    console.error(error);
    res.status(500).send("Server Error.");
  }
});

/**
 * @swagger
 * /user/{username}/classes/{slug}:
 *  put:
 *    summary: Update a class of a teacher.
 *    description: Teacher account required.
 *    tags:
 *      - classes
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *      - name: slug
 *        in: path
 *        required: true
 *        description: Class slug
 *        schema:
 *          type: string
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              class_name:
 *                type: string
 *              class_description:
 *                type: string
 *    responses:
 *      '200':
 *        description: Created
 *      '400':
 *        description: Invalid body
 */
router.put("/:slug", tokenCheck, userCheck, teacherCheck, async (req, res) => {
  try {
    const slug = req.params.slug;
    const teacherUUID = req.userUUID;

    // Get the old class.
    let query = `
      SELECT class_name, class_description, slug
      FROM classes
      WHERE (teacher_uuid=$1 AND slug=$2)
    `;

    let result = await db.query(query, [teacherUUID, slug]);

    if (result.rowCount === 0) {
      return res.status(400).json("No class found.");
    }

    // Preserve the old details.
    const className = req.body.class_name;
    const classDescription = req.body.class_description;

    // Update the class.
    query = `
      UPDATE classes
      SET class_name = $1, class_description = $2
      WHERE (slug = $3 AND teacher_uuid = $4)
      RETURNING *
    `;

    result = await db.query(query, [
      className,
      classDescription,
      slug,
      teacherUUID,
    ]);

    res.json(result.rows[0]);
  } catch (error) {
    console.error(error);
    res.status(500).send("Server Error");
  }
});

/**
 * @swagger
 * /user/{username}/classes/{slug}:
 *  delete:
 *    summary: Delete a class of a teacher.
 *    description: Account required.
 *    tags:
 *      - classes
 *    parameters:
 *      - name: username
 *        in: path
 *        required: true
 *        description: User name of the teacher.
 *        schema:
 *          type: string
 *      - name: slug
 *        in: path
 *        required: true
 *        description: Class slug
 *        schema:
 *          type: string
 *    responses:
 *      '200':
 *        description: Ok
 *      '400':
 *        description: Invalid Params
 */
router.delete(
  "/:slug",
  tokenCheck,
  userCheck,
  teacherCheck,
  async (req, res) => {
    try {
      const slug = req.params.slug;
      const teacherUUID = req.userUUID;

      // Delete this class.
      const query = `
        DELETE FROM classes
        WHERE (slug = $1 AND teacher_uuid = $2)
      `;

      const result = await db.query(query, [slug, teacherUUID]);

      if (result.rowCount === 0) {
        return res.status(400).json("Class does not exist.");
      }

      res.json("Class deleted successfully.");
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error.");
    }
  }
);

router.use("/:slug/students", slugParam, studentRouter);

export default router;
