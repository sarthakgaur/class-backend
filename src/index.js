import express from "express";
import morgan from "morgan";
import swaggerUI from "swagger-ui-express";
import swaggerJSDoc from "swagger-jsdoc";
import dotenv from "dotenv";

import usernameParam from "./middlewares/usernameParam.js";
import authRouter from "./routes/auth.js";
import classesRouter from "./routes/classes.js";
import registrationsRouter from "./routes/registrations.js";

const app = express();
dotenv.config();

// Middleware
app.use(express.json());

// Logger Setup
app.use(morgan("common"));

// Swagger Setup
const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Class API",
    },
  },
  apis: ["./src/routes/*.js"],
};

const swaggerSpec = swaggerJSDoc(options);

app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerSpec));

// Register and login routes.
app.use("/auth", authRouter);

// Classes routes.
app.use("/user/:username/classes", usernameParam, classesRouter);

// Registration routes.
app.use("/user/:username/registrations", usernameParam, registrationsRouter);

const port = process.env.SERVER_PORT || 3000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}.`);
});
