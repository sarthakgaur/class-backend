import * as db from "../db/db.js";

export default async function getStudentDetails(condition, params) {
  const query = `
    SELECT
      reg_uuid,
      class_name,
      teachers.user_name AS teacher_user_name,
      stu.user_name AS student_user_name,
      grade,
      remarks
    FROM
    (
      SELECT * FROM student_registrations
      WHERE (${condition})
    ) AS temp_table
    JOIN classes ON temp_table.class_uuid=classes.class_uuid
    JOIN users as teachers ON classes.teacher_uuid=teachers.user_uuid
    JOIN users AS stu ON temp_table.student_uuid=stu.user_uuid;
  `;

  return await db.query(query, params);
}
