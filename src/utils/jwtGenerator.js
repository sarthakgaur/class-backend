import jwt from "jsonwebtoken";
import dotenv from 'dotenv';

dotenv.config()

export default function jwtGenerator(userUUID) {
  const payload = {
    userUUID,
  };

  return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: "1hr" });
}
