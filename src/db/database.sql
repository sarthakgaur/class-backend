CREATE DATABASE class;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE permissions(
    perm_id BIGSERIAL PRIMARY KEY, 
    perm_module VARCHAR(255) NOT NULL,
    perm_description TEXT NOT NULL
);

CREATE TABLE roles(
    role_id BIGSERIAL PRIMARY KEY,
    role_name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE roles_permissions(
    role_id BIGINT NOT NULL,
    perm_id BIGINT NOT NULL,

    PRIMARY KEY (role_id, perm_id),
    CONSTRAINT fk_role_id
        FOREIGN KEY(role_id)
        REFERENCES roles(role_id)
        ON DELETE CASCADE,
    CONSTRAINT fk_perm_id
        FOREIGN KEY(perm_id)
        REFERENCES permissions(perm_id)
        ON DELETE CASCADE
);

CREATE TABLE users(
  user_uuid uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  user_name VARCHAR(255) UNIQUE NOT NULL,
  user_email VARCHAR(255) UNIQUE NOT NULL,
  user_password VARCHAR(255) NOT NULL,
  role_id BIGINT,

  CONSTRAINT fk_role_id
    FOREIGN KEY(role_id)
    REFERENCES roles(role_id)
    ON DELETE SET NULL
);

CREATE TABLE classes(
    class_uuid uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    class_name VARCHAR(255) NOT NULL,
    class_description TEXT NOT NULL,
    slug VARCHAR(255) NOT NULL,
    teacher_uuid uuid NOT NULL,

    UNIQUE (teacher_uuid, slug),
    CONSTRAINT fk_teacher_uuid
        FOREIGN KEY(teacher_uuid)
        REFERENCES users(user_uuid)
        ON DELETE CASCADE
);

CREATE TABLE student_registrations(
    reg_uuid uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    class_uuid uuid NOT NULL,
    student_uuid uuid NOT NULL,
    grade VARCHAR(10),
    remarks TEXT,

    UNIQUE (student_uuid, class_uuid),
    CONSTRAINT fk_student_uuid
        FOREIGN KEY(student_uuid)
        REFERENCES users(user_uuid)
        ON DELETE CASCADE,
    CONSTRAINT fk_class_uuid
        FOREIGN KEY(class_uuid)
        REFERENCES classes(class_uuid)
        ON DELETE CASCADE
);

INSERT INTO permissions (perm_module, perm_description) VALUES
('teacher', 'create class'),
('teacher', 'update class'),
('teacher', 'delete class'),

('teacher', 'read grade'),
('teacher', 'assign grade'),
('teacher', 'update grade'),
('teacher', 'delete grade'),

('teacher', 'read remarks'),
('teacher', 'assign remarks'),
('teacher', 'update remarks'),
('teacher', 'delete remarks'),

('teacher', 'read student'),
('teacher', 'assign student'),
('teacher', 'update student'),
('teacher', 'delete student');

INSERT INTO roles (role_name) VALUES
('admin'),
('teacher'),
('student');

INSERT INTO roles_permissions (role_id, perm_id) VALUES
(1, 1), (1, 2), (1, 3),
(1, 4), (1, 5), (1, 6), (1, 7),
(1, 8), (1, 9), (1, 10), (1, 11),
(1, 12), (1, 13), (1, 14), (1, 15),

(2, 1), (2, 2), (2, 3),
(2, 4), (2, 5), (2, 6), (2, 7),
(2, 8), (2, 9), (2, 10), (2, 11),
(2, 12), (2, 13), (2, 14), (2, 15);
