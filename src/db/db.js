import pg from "pg";
import dotenv from "dotenv";

dotenv.config();
const Pool = pg.Pool;

export const pool = new Pool({
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT),
  database: process.env.DB_DATABASE,
});

export async function query(text, params) {
  const start = Date.now();
  const results = await pool.query(text, params);
  const duration = Date.now() - start;
  console.log("Executed query:", {
    text,
    params,
    duration,
    rowCount: results.rowCount,
    firstRow: results.rows[0],
  });
  return results;
}
