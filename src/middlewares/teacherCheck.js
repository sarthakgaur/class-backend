import * as db from '../db/db.js';
import ROLES from '../data/roles.js';

export default async function teacherCheck(req, res, next) {
  try {
    const userUUID = req.userUUID;

    const result = await db.query("SELECT * FROM users WHERE user_uuid = $1", [
      userUUID,
    ]);

    if (result.rows[0].role_id === ROLES.TEACHER) {
      next();
    } else if (result.rows[0].role_id === ROLES.ADMIN) {
      next();
    } else {
      return res.status(403).json("Not Authorized");
    }
  } catch (error) {
    console.error(error);
    return res.status(403).json("Not Authorized");
  }
}
