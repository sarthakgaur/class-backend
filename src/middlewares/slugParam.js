export default function slugParam(req, res, next) {
  req.slug = req.params.slug;
  next();
}
