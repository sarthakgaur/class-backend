export default function usernameParam(req, res, next) {
  req.username = req.params.username;
  next();
}
