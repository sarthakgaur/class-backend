import jwt from 'jsonwebtoken';
import dotenv from 'dotenv'

dotenv.config();

export default async function tokenCheck(req, res, next) {
  try {
    const jwtToken = req.header("token");

    if (!jwtToken) {
      return res.status(403).json("Not Authorized");
    }

    const payload = jwt.verify(jwtToken, process.env.JWT_SECRET);
    req.userUUID = payload.userUUID;
    next();
  } catch (error) {
    console.error(error);
    return res.status(403).json("Not Authorized");
  }
}
