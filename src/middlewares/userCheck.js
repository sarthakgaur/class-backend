import * as db from "../db/db.js";
import ROLES from "../data/roles.js";

/**
 * Call this function after calling usernameParam and tokenCheck.
 */
export default async function userCheck(req, res, next) {
  try {
    const username = req.username;
    const userUUID = req.userUUID;

    const result = await db.query("SELECT * FROM users WHERE user_uuid = $1", [
      userUUID,
    ]);

    if (result.rows[0].user_name === username) {
      next();
    } else if (result.rows[0].role_id === ROLES.ADMIN) {
      next();
    } else {
      return res.status(403).json("Not Authorized");
    }
  } catch (error) {
    console.error(error);
    return res.status(403).json("Not Authorized");
  }
}
