const ROLES = {
  ADMIN: "1",
  TEACHER: "2",
  STUDENT: "3",
};

export default ROLES;
