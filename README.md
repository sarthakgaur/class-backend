# class-backend
Backend for a class management system built using Express, Node, and PostgreSQL.

## Installation

### Clone The Project
1. `git clone https://github.com/sarthakgaur/class-backend`

### Database Setup
1. Install PostgreSQL
2. Start the PostgreSQL server
3. Execute all the SQL commands in `class-backend/src/db/database.sql`

### Backend Setup
1. `cd class-backend`
2. `npm install`
3. `touch .env`
4. Enter the following content in the `.env` file:

        DB_USER=user
        DB_PASSWORD=password
        DB_HOST=localhost
        DB_PORT=5432
        DB_DATABASE=class

        SERVER_PORT=3000
        JWT_SECRET=secret

### Running the Server
1. `cd class-backend`
2. `node src/index.js`

## Important
The documentation for the APIs is available at `/docs`.
